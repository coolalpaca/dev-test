/*
	Testing is good. It's hard to do, and hard to keep up, but it's good.

	We're delinquent throughout, but I feel most comfortable with the modules
	that are well-tested.

	Testing angular is a little tricky, as it requires running a browser (PhantomJS),
	but so far I'm using Jasmine - which can also be used for node modules, which 
	is a little bit simpler.

	You'll need to install jasmine-node, and some node modules (defined in package.json):
		`npm install`
		`npm install -g jasmine-node`

	After that, you can run tests with: 
		`jasmine-node testing.spec.js`

	This task has a function called "roundHalf" that takes an input value and an optional
	truthy/falsey, and rounds the value to the nearest increment of 0.5. The function will round 
	only up by default, unless the roundUp parameter is false (not undfined).

	1. Using the test 'should produce the correct value when rounding up', write the tests for
	'should produce the correct value when rounding both ways'. (ie: add false as the second parameter
	to roundHalf).

	2. Write a new test to check that roundHalf returns 0 when the input value is undefined, null, or false.
	
*/

function roundHalf(value, roundUp) {
	if (value == 0) {
		return 0;
	}

	if (typeof roundUp == "undefined") {
		roundUp = true;
	}

	if (roundUp) {
		var r = Math.ceil(value*2)/2;
	}
	else {
		var r = Math.round(value*2)/2;	
	}

	if (isNaN(r)) {
		return 0;
	}
	
	return r;
}

describe('roundHalf(value, roundUp)', function() {
	it('should exist', function() {
		expect(roundHalf).toBeDefined();
	});

	it('should produce the correct value for 0', function() {
		expect(roundHalf(0)).toEqual(0);
	});

	it('should produce the correct value for whole numbers', function() {
		expect(roundHalf(1)).toEqual(1);
		expect(roundHalf(2)).toEqual(2);
		expect(roundHalf(2000)).toEqual(2000);
	});

	it('should produce the correct value for strings', function() {
		expect(roundHalf("0")).toEqual(0);
		expect(roundHalf("1")).toEqual(1);
		expect(roundHalf("2000")).toEqual(2000);
		expect(roundHalf("hello")).toEqual(0);
	});

	
	it('should produce the correct value when rounding up', function() {
		expect(roundHalf(0.000001)).toEqual(0.5);
		expect(roundHalf(0.249999)).toEqual(0.5);
		expect(roundHalf(0.250000)).toEqual(0.5)
		expect(roundHalf(0.250001)).toEqual(0.5)
		expect(roundHalf(0.499999)).toEqual(0.5)
		expect(roundHalf(0.500000)).toEqual(0.5)
		expect(roundHalf(0.500001)).toEqual(1)
		expect(roundHalf(0.749999)).toEqual(1)
		expect(roundHalf(0.750000)).toEqual(1)
		expect(roundHalf(0.750001)).toEqual(1)
		expect(roundHalf(1.000000)).toEqual(1);
	});

	it('should produce the correct value when rounding both ways', function() {
		/* Answer to first question goes here */
	});

	var testCases = [
		[0, false, 0],
		[0.2, false, 0],
		[0.4999, false, 0.5],
		[0.51, false, 0.5],
		["0.2", false, 0],
		["1", false, 1],
		[0, true, 0],
		[0.2, true, 0.5],
		[0.4999, true, 0.5],
		[0.51, true, 1],
	];

	testCases.forEach(function(t) {
		it('should produce the correct output for (' + t[0] + ', ' + t[1] + ' = '+t[2] + ')', function() {
			expect(roundHalf(t[0], t[1])).toEqual(t[2]);
		});
	});
});
