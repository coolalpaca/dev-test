/*
	The purpose of this task is to do some fun stuff with functional javascript.

	Use the "data" array below to produce the requested outputs.

	It's probably easiest to just run it with nodejs (`node functional.js`), but 
	there's no reason why it wouldn't work in the browser (I don't think).

	Try to do these as succinctly as possible - using filter, map, forEach, reduce, and join.

	If you need to need to break out of the functional paradigm (ie: start using
	for loops, etc) it's not a huge deal, but keep in mind that you shouldn't need to.

	It's (pretty much) always possible to use a forEach instead of a reduce by using
	variables outside of the function (or globals). Not a big deal, but avoid it if 
	you can for this exercise.

	(If all else fails. Do what you need to do to get the expected outout.)

	Example (See answer below): Produce the total age of everyone with only 2 favourites

		Expected Output: 70

	1. Produce a comma-separated string listing the people who like Blue:
		
		Expected Output: 'Bob Johnson, Steve Sullivan, Jack Dibbs'

	2. Produce a comma-separated string listing the number of people who 
		like each color - along with the counts of each. AND - only show
		those that have more than 1 "favouriter":
		
		Expected Output: 'Blue (3), Green (2), Red (2), Orange (1), Black (1)'

*/
var data = [
	{
		name: 'Bob Johnson',
		age: 26,
		favourites: [
			'Blue',
			'Green'
		]
	},
	{
		name: 'Steve Sullivan',
		age: 55,
		favourites: [
			'Blue',
			'Red',
			'Orange'
		]
	},
	{
		name: 'Jack Dibbs',
		age: 14,
		favourites: [
			'Black',
			'Blue'
		]
	},
	{
		name: 'George Plant',
		age: 30,
		favourites: [
			'Red',
			'Green'
		]
	}
]

/* Example Answer: */
var output = data.filter(function(person) {
	return person.favourites.length == 2;
}).reduce(function(totalAge, person) {
	return totalAge + person.age;
}, 0);
console.log(output);

