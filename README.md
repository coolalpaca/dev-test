# README #

These tasks are designed to be a quick test of a developer. They should be fairly easy with the right knowledge, and not too much more difficult without. 

Feel free to use whatever resources you'd have available to you on any other day. It won't benefit anyone if you don't complete them yourself.

Each task should be able to be completed independently, so if you're stuck on one - don't worry too much.

If you run into something preventing you from completing a task, ask for help. DEFINITELY ask for help if you're having difficulty that's preventing you working on them at all (ie: can't get code checked out, can't get packages installed).

When you're complete, either make a pull request to the repository with your new code - or just zip it up and send your answers.

There are 4 tasks. Further instructions & hints are located in the files themselves. 

The files are:

- functional.js
- promises.js
- testing.js
- angular.html