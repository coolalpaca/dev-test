/* 

  We use a lot of promises to deal with asynchronous requests. 

  Basically any request to the PouchDB database is going to be promise-based,
  meaning that it's callers will be as well.

  This task has a function called getData, that produces some nonsensical, random data.

  It's promise is resolved or rejected based on a random value. It passes an object with 
  some random values to the next function in the chain.

  1. Call getData, then print it's results. Just print "error!" if there's an error.

  2. Given the following function (getData), write another function that calls getData, and returns a promise that will be resolved if the given function's results have the field "foobar"

  3. Call getData 3 times, sleeping for 3 seconds between each. You can use the "sleep" function defined below, which returns a promise. Bonus points for using Promise.all().

*/

var Promise = require('bluebird');

var getData = function() {
  return new Promise(function (resolve, reject) {
    var d = new Date();
    var n = d.getTime();
    var s = n.toString();
    var r = {
      barfoo: true
    }

    if (!(s[s.length-1] % 5)) {
      return reject({error: "That's wrong!"});
    }

    if (s[s.length-1] % 2) {
      r.foobar = s;
    }

    return resolve(r);
  });
};

var sleep = function(time) {
  return new Promise(function(resolve, reject) {
    setTimeout(resolve, time);
  })
}


